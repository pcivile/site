<!DOCTYPE html>
<html>
<head>
	<title>Aggiungi Volontario</title>
	<!--Import Google Icon Font-->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!--Import materialize.css-->
	<link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
	<!--Let browser know website is optimized for mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <script src="jQuery.js"></script>
    <script type="text/javascript" src="js/materialize.js"></script>
    <script type="text/javascript">
    	$(document).ready(function(){
			$('select').formSelect();
		});
		$(document).ready(function(){
			M.updateTextFields();
		});
    </script>
</head>
<body>
	<div class="navbar-fixed">
    <nav>
      <div class="nav-wrapper">
        <a href="#!" class="brand-logo center">Protezione Civile</a>
      </div>
    </nav>
  </div>
	<?php
    if($_POST)
    {
        $servername = "localhost";
        $username = "protezionec";
        $password = "sBab9dfJXhKD";
        $dbname = "my_protezionec";
        $conn = new mysqli($servername, $username, $password, $dbname);
        $nome = $_POST['nome'];
        $cognome = $_POST['cognome'];
        $cap = 0;
        if($_POST['caposquadra'] == "on") $cap = 1;
        $idcap = $_POST['idcaposquadra'];
        $loc = $_POST['localita'];
        $disp = $_POST['disponibilita'];
        $user = $_POST['username'];
        $psw = $_POST['password'];
        $tel = $_POST['telefono'];
        //echo($_POST['nome']." ".$_POST['cognome']." ".$_POST['caposquadra']." ".$_POST['idcaposquadra']." ".$_POST['localita']." ".$_POST['disponibilita']);
        $result = mysqli_query($conn,"SELECT MAX(ID) FROM Volontari");
        $row = mysqli_fetch_array($result);
        $idmax = $row[0];
        $query = "INSERT INTO Volontari (Nome,Cognome,Caposquadra,ID_caposquadra,Localita,Disponibilita,Token) VALUES ('".$nome."','".$cognome."',".$cap.",".$idcap.",'".$loc."',".$disp.",null)";
        $query2 ="INSERT INTO Login (ID,Telefono,Username,Password) VALUES (".$idmax.",".$tel.",'".$user."','".$psw."')";
        mysqli_query($conn, $query2);
      	if (mysqli_query($conn, $query))
        {
        	echo"<script>alert('Volontario inserito con successo!');</script>";
        }
        else echo "<script>alert('Errore nel salvataggio: <br>".mysqli_error($conn)."');</script>";
		mysqli_close($conn);
        echo"<br><br><br><br><br><br>
        	<div class='row'>
        	<center>
        	<a href='index.php' class='waves-effect waves-light btn'>TORNA A GESTIONE DIPENDENTI</a>
            </center>
        	</div>";
    }
    else
    {
        echo "
        <div class='container'>
        <div class='row'>
		    <form class='col s12' action='aggiungi_volontario.php' method='POST'>
		    	<div class='row'>
		        	<div class='input-field col s6'>
			        	<input id='first_name' type='text' class='validate' required='' aria-required='true' name='nome'>
			        	<label for='first_name'>Nome</label>
			        </div>
			        <div class='input-field col s6'>
			        	<input id='last_name' type='text' class='validate' required='' aria-required='true' name='cognome'>
			        	<label for='last_name'>Cognome</label>
			        </div>
		     	</div>
		     	<div class='row'>
				    <div class='input-field col s6'>
						<select name='disponibilita'>
					    	<option value=0>Non disponibile</option>
					    	<option value=1>Disponibile</option>
					    	<option value=2 selected>Non data</option>
					    </select>
					   	<label>Disponibilit&agrave</label>
					</div>
					<p>
				      	<label>
					    	<input type='checkbox' class='filled-in' name='caposquadra'/>
					    	<span>Caposquadra</span>
					  	</label>
			      	</p>
				</div>
				<div class='row'>
					<div class='input-field col s6'>
			        	<input id='localita' type='text' class='validate' required='' aria-required='true' name='localita'>
			        	<label for='localita'>Localita</label>
			        </div>
			        <div class='input-field col s6'>
			        	<input id='idcaposquadra' type='number' class='validate' required='' aria-required='true' name='idcaposquadra'>
			        	<label for='idcaposquadra'>ID Caposquadra</label>
			        </div>
				</div>
                <div class='row'>
					<div class='input-field col s6'>
			        	<input id='username' type='text' class='validate' required='' aria-required='true' name='username'>
			        	<label for='username'>Username</label>
			        </div>
                   <div class='input-field col s6'>
			        	<input id='psw' type='password' class='validate' required='' aria-required='true' name='password'>
			        	<label for='psw'>Password</label>
			        </div>
                     <div class='input-field col s6'>
			        	<input id='telefono' type='number' name='telefono'>
			        	<label for='telefono'>Numero di Telefono</label>
			        </div>
                    <div class='input-field col s6'>
                    	<button class='btn waves-effect waves-light' type='submit' name='action'>Submit<i class='material-icons right'>send</i>
						</button>	
			        </div>
				
		    </form>
			</div>
		</div>
        ";
    }
    ?>
</body>
</html>
