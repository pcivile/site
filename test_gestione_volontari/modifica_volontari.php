<!DOCTYPE html>
<html>
<head>
	<title>Modifica Volontario</title>
    <!--Import Google Icon Font-->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!--Import materialize.css-->
	<link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
	<!--Let browser know website is optimized for mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <script src="jQuery.js"></script>
    <script type="text/javascript" src="js/materialize.js"></script>
	<script type="text/javascript">
    	$(document).ready(function(){
			$('select').formSelect();
		});
    </script>
</head>
<body>
	<div class="navbar-fixed">
    	<nav>
      		<div class="nav-wrapper">
        		<a href="#!" class="brand-logo center">Protezione Civile</a>
      		</div>
    	</nav>
  	</div>
	<?php
    if($_POST)
    {
        $servername = "localhost";
        $username = "protezionec";
        $password = "sBab9dfJXhKD";
        $dbname = "my_protezionec";
        $conn = new mysqli($servername, $username, $password, $dbname);
        $id = $_POST['id'];
        $nome = $_POST['nome'];
        $cognome = $_POST['cognome'];
        $cap = 0;
        if($_POST['caposquadra'] == "on") $cap = 1;
        $idcap = $_POST['idcaposquadra'];
        $loc = $_POST['localita'];
        $disp = $_POST['disponibilita'];
        $query = "UPDATE Volontari SET Nome='".$nome."', Cognome='".$cognome."', Caposquadra=".$cap.", ID_caposquadra=".$idcap.", Localita='".$loc."', Disponibilita=".$disp." WHERE ID=".$id;
      
        if (mysqli_query($conn, $query) == true)echo"<script>alert('Modifica salvata con successo!');</script>"; 
        else echo "<script>alert('Errore nel salvataggio: <br>".mysqli_error($conn)."');</script>";
		mysqli_close($conn);
        echo"<br><br><br><br><br><br>
        	<div class='row'>
        	<center>
        	<a href='index.php' class='waves-effect waves-light btn'>TORNA A GESTIONE DIPENDENTI</a>
            </center>
        	</div>";
    }
    elseif ($_GET)
    {
    	$servername = "localhost";
        $username = "protezionec";
        $password = "sBab9dfJXhKD";
        $dbname = "my_protezionec";
        // Create connection
        $conn = new mysqli($servername, $username, $password, $dbname);
    	$id = $_GET["id"];
        //echo "$id";
        $result = mysqli_query($conn,"SELECT * FROM Volontari WHERE ID = $id");
        $row = mysqli_fetch_row($result);
        //$json = json_encode($row);
        //print_r($json);	//debug
        mysqli_close($conn);
        echo "
        <div class='container'>
        <div class='row'>
		    <form class='col s12' action='modifica_volontari.php' method='POST'>
		    	<input type='hidden' name='id' value='".$row[0]."'>
		    	<div class='row'>
		        	<div class='input-field col s6'>
			        	<input id='first_name' type='text' class='validate' required='' aria-required='true' name='nome' value='".$row[1]."'>
			        	<label for='first_name'>Nome</label>
			        </div>
			        <div class='input-field col s6'>
			        	<input id='last_name' type='text' class='validate' required='' aria-required='true' name='cognome' value='".$row[2]."'>
			        	<label for='last_name'>Cognome</label>
			        </div>
		     	</div>
		     	<div class='row'>
				    <div class='input-field col s6'>
						<select name='disponibilita'>";
					    	if($row[6] == 0) echo"<option selected value=0>Non disponibile</option>"; else echo"<option value=0>Non Disponibile</option>";
        					if($row[6] == 1) echo"<option selected value=1>Disponibile</option>"; else echo"<option value=1>Disponibile</option>";
        					if($row[6] == 2) echo"<option selected value=2>Non Data</option>"; else echo"<option value=2>Non Data</option>";
					echo"</select>
					   	<label>Disponibilit&agrave</label>
					</div>
					<p>
				      	<label>";
					    if($row[3] == 1) echo "<input type='checkbox' class='filled-in' name='caposquadra' checked='checked'/>";
					    else echo "<input type='checkbox' class='filled-in' name='caposquadra'/>";
					    echo"<span>Caposquadra</span></label>
			      	</p>
				</div>
				<div class='row'>
					<div class='input-field col s6'>
			        	<input id='localita' type='text' class='validate' required='' aria-required='true' name='localita' value='".$row[5]."'>
			        	<label for='localita'>Localita</label>
			        </div>
			        <div class='input-field col s6'>
			        	<input id='idcaposquadra' type='number' class='validate' required='' aria-required='true' name='idcaposquadra' value='".$row[4]."'>
			        	<label for='idcaposquadra'>ID Caposquadra</label>
			        </div>
				</div>
				<button class='btn waves-effect waves-light' type='submit' name='action'>Submit<i class='material-icons right'>send</i>
				</button>
		    </form>
		</div>
        </div>
        ";
    }
    ?>
</body>
</html>
