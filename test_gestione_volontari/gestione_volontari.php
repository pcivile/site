<!DOCTYPE html>
<html>
<head>
	<title>Gestione Volontari</title>
    <script src="jQuery.js"></script>
    <script>
    	function deleteRow(buttonid) 
        {
          if (confirm("Vuoi davvero eliminare questo campo?"))
          {
          	$.post("elimina_volontari.php",{name: buttonid},
            function succ(data)
            {
            	alert("Eliminazione eseguita con successo risp:" + data);
                location.reload();
            });
          }
          else alert("Operazione annullata!");
      	}
    </script>
    <style>
    	body{
        	background-color: #ffffff;
        }
        table{
        	background-color: #ffffff;
        }
        table tr:nth-child(odd){
        	background-color: #d9d9d9;
        }
        img{
        	width: 30px;
            height: 30px;
        }
    </style>
</head>
<body>
	<div id="overview" style="position: absolute; left: 10%; top: 10%; overflow: auto;">
		<?php
        //Davide Monti 5Info1
		include("connessione_db.php");
        $result = mysqli_query($conn,"SELECT * FROM Volontari ORDER BY ID");
        $dim = mysqli_num_rows($result);
        while($row = $result->fetch_assoc())  $rows[] = $row;  
        //$json = json_encode($rows);
        //print_r($json);	//debug
        $table = "<table border = 1><tr><td>ID</td><td>Nome</td><td>Cognome</td><td>Caposquadra</td><td>ID_Caposquadra</td><td>Localit&agrave</td><td>Disponibilit&agrave</td><td>Token</td><td colspan=2>Azioni</td>";
                for ($riga = 0; $riga < $dim; $riga++)
				{
                    $table = $table . "<tr><td> ".$rows[$riga]["ID"]."</td><td> ".$rows[$riga]["Nome"]."</td><td> ".$rows[$riga]["Cognome"]."</td><td> ".$rows[$riga]["Caposquadra"]."</td><td> ".$rows[$riga]["ID_caposquadra"]."</td><td> ".$rows[$riga]["Localita"]."</td><td> ".$rows[$riga]["Disponibilita"]."</td><td> ".$rows[$riga]["Token"]."</td><td><a href='modifica_volontari.php?id=".$rows[$riga]["ID"]."&nome=".$rows[$riga]["Nome"]."' style='color: black;'><img src='penna.png'/></a></td><td class='delete'><button id=".$rows[$riga]["ID"]." onclick='deleteRow(this.id);'><img src='cestino.png'/></button></td></tr>";
				}
			echo $table."</table>";
		?>
	</div>
	<div style="position: absolute; right: 10%; top: 10%; max-width: 50%;">
		<table border=1>
			<tr>
				<td><a href="aggiungi_volontario.php" style="color: black;">Aggiungi Volontario</a></td>
			</tr>
		</table>
	</div>
</body>
</html>

