<!DOCTYPE html>
<html>
<head>
    
<!--Import Google Icon Font-->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!--Import materialize.css-->
  <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>

  <!--Let browser know website is optimized for mobile-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<title>Squadre</title>
    <script src="jQuery.js"></script>
    <script type="text/javascript" src="js/materialize.js"></script>
    <script>

        $(document).ready(function(){
            $('.fixed-action-btn').floatingActionButton();
        });
        
        $(document).ready(function(){
          $('.collapsible').collapsible();
        });

    	function deleteRow(buttonid) 
        {
          if (confirm("Vuoi davvero eliminare questo campo?"))
          {
          	$.post("elimina_volontari.php",{name: buttonid},
            function succ(data)
            {
            	alert("Eliminazione eseguita con successo risp:" + data);
                location.reload();
            });
          }
          else alert("Operazione annullata!");
      	}
    </script>
    <style>
    	body{
        	background-color: #ffffff;
        }
        table{
        	background-color: #ffffff;
        }
        table tr:nth-child(odd){
        	background-color: #d9d9d9;
        }
        img{
        	width: 30px;
            height: 30px;
        }
    </style>
</head>
<body>
    <div class="navbar-fixed">
    <nav>
      <div class="nav-wrapper">
        <a href="#!" class="brand-logo center">Protezione Civile</a>
        <ul class="right hide-on-med-and-down">
          <li>
            <a href="../index.html">Homepage</a>
          </li>
          <li>
            <a href="index.php">Gestione</a>
          </li>
        </ul>
      </div>
    </nav>
  </div>
  <div class="fixed-action-btn">
  <a class="btn-floating btn-large teal" href="">
    <i class="large material-icons">add</i>
  </a>
</div>
  <div class="container">
	<div id="overview">
		<?php
        //Davide Monti 5Info1
		include("connessione_db.php");
        $result = mysqli_query($conn,"SELECT * FROM Squadre ORDER BY ID_Squadra");
        $dim = mysqli_num_rows($result);
        while($row = $result->fetch_assoc())  $teamrows[] = $row;
        //$json = json_encode($teamrows);
        //print_r($json);	//debug
        echo "<ul class='collapsible'>";
        for ($riga = 0; $riga < $dim; $riga++)
        {
        	echo "<li><div class='collapsible-header'><i class='material-icons'>group</i>".$teamrows[$riga]["Nome"]."</div>";
            echo "<div class='collapsible-body'>";
            echo "<div class='container'>";
        	$result2 = mysqli_query($conn,"SELECT ID,ID_Caposquadra,Nome,Cognome,Caposquadra,Disponibilita FROM Volontari WHERE ID_caposquadra = ".$teamrows[$riga]["ID_Squadra"]."");
            $dim2 = mysqli_num_rows($result2);
            while($row2 = $result2->fetch_assoc())  $rows2[] = $row2;
            //$json = json_encode($rows2);
            //print_r($json);
            $table= "";
            $table = "<table border = 1><tr><th>Nome</th><th>Cognome</th><th>Caposquadra</th><th>Disponibilit&agrave</th><th>Azioni</th></tr>";
            for($riga2 = 0; $riga2 < $dim2; $riga2++)
            {
            		$disp = '';
                    if ($rows2[$riga2]["Disponibilita"] == 0) $disp = 'Non Disponibile';
                    if ($rows2[$riga2]["Disponibilita"] == 1) $disp = 'Disponibile';
                    if ($rows2[$riga2]["Disponibilita"] == 2) $disp = 'Non Data';
                    $cap='';
                    if ($rows2[$riga2]["Caposquadra"] == 0) $cap = 'Membro';
                    if ($rows2[$riga2]["Caposquadra"] == 1) $cap = 'Caposquadra';
                    $table = $table."<tr>
                        <td> ".$rows2[$riga2]["Nome"]."</td>
                        <td> ".$rows2[$riga2]["Cognome"]."</td>
                        <td> ".$cap."</td>
                        <td> ".$disp."</td>
                        <td>
                            <a href='modifica_volontari.php?id=".$rows2[$riga2]["ID"]."' class='waves-effect waves-light btn'>
                                <i class='material-icons'>edit</i>
                            </a>
                        </td>";
            }
            echo $table."</table></div></div></li>";
            $rows2 = null;
        }
        echo "</ul>";
		?>
	</div>
    </div>
    
</body>
</html>

