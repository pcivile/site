<!DOCTYPE html>
<html>
<head>
    
<!--Import Google Icon Font-->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!--Import materialize.css-->
  <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>

  <!--Let browser know website is optimized for mobile-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<title>Gestione Volontari</title>
    <script src="jQuery.js"></script>
    <script type="text/javascript" src="js/materialize.js"></script>
    <script>

        $(document).ready(function(){
            $('.fixed-action-btn').floatingActionButton();
        });

    	function deleteRow(buttonid) 
        {
          if (confirm("Vuoi davvero eliminare questo campo?"))
          {
          	$.post("elimina_volontari.php",{name: buttonid},
            function succ(data)
            {
            	alert("Eliminazione eseguita con successo risp:" + data);
                location.reload();
            });
          }
          else alert("Operazione annullata!");
      	}
    </script>
    <style>
    	body{
        	background-color: #ffffff;
        }
        table{
        	background-color: #ffffff;
        }
        table tr:nth-child(odd){
        	background-color: #d9d9d9;
        }
        img{
        	width: 30px;
            height: 30px;
        }
    </style>
</head>
<body>
    <div class="navbar-fixed">
    <nav>
      <div class="nav-wrapper">
        <a href="#!" class="brand-logo center">Protezione Civile</a>
        <ul class="right hide-on-med-and-down">
          <li>
            <a href="../index.html">Homepage</a>
          </li>
          <li>
            <a href="teams_index.php">Squadre</a>
          </li>
        </ul>
      </div>
    </nav>
  </div>
  <div class="fixed-action-btn">
  <a class="btn-floating btn-large teal" href="aggiungi_volontario.php">
    <i class="large material-icons">add</i>
  </a>
</div>
  <div class="container">
	<div id="overview">
		<?php
        //Davide Monti 5Info1
		include("connessione_db.php");
        $result = mysqli_query($conn,"SELECT * FROM Volontari ORDER BY ID");
        $dim = mysqli_num_rows($result);
        while($row = $result->fetch_assoc())  $rows[] = $row;
        //$json = json_encode($rows);
        //print_r($json);	//debug
        $table = "<table border = 1><tr><th>ID</th><th>Nome</th><th>Cognome</th><th>Caposquadra</th><th>ID_Caposquadra</th><th>Localit&agrave</th><th>Disponibilit&agrave</th><th colspan=2>Azioni</th></tr>";
                for ($riga = 0; $riga < $dim; $riga++)
				{
                	$disp = '';
                    if ($rows[$riga]["Disponibilita"] == 0) $disp = 'Non Disponibile';
                    if ($rows[$riga]["Disponibilita"] == 1) $disp = 'Disponibile';
                    if ($rows[$riga]["Disponibilita"] == 2) $disp = 'Non Data';
                    $cap='';
                    if ($rows[$riga]["Caposquadra"] == 0) $cap = 'No';
                    if ($rows[$riga]["Caposquadra"] == 1) $cap = 'S&igrave';
                    $table = $table . "<tr>
                    	<td>".$rows[$riga]["ID"]."</td>
                        <td> ".$rows[$riga]["Nome"]."</td>
                        <td> ".$rows[$riga]["Cognome"]."</td>
                        <td> ".$cap."</td>
                        <td> ".$rows[$riga]["ID_caposquadra"]."</td>
                        <td> ".$rows[$riga]["Localita"]."</td>
                        <td> ".$disp."</td>
                        <td>
                            <a href='modifica_volontari.php?id=".$rows[$riga]["ID"]."&nome=".$rows[$riga]["Nome"]."' class='waves-effect waves-light btn'>
                                <i class='material-icons'>edit</i>
                            </a>
                        </td>
                        <td class='delete'>
                            <a id=".$rows[$riga]["ID"]." onclick='deleteRow(this.id);'
                                class='waves-effect waves-light red btn'>
                            <i class='material-icons'>delete</i>
                                </a>
                        </td>
                    </tr>";
				}
            echo "<br>";
			echo $table."</table>";
		?>
	</div>
    </div>
    
</body>
</html>

