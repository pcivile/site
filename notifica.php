<?php

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "{\r\n    \"to\" : \"cAmk1bXKe3k:APA91bESIs4gH9Ne-P1HIi3ouW9dIni7H68CatSwphBjH9G1RVgqItkcgEVZXllHUV7i_GdVLIysfxm1NSJcglAf\",\r\n    \"notification\" : {\r\n        \"body\" : \"This is an notification message!\",\r\n        \"title\" : \"Message\"\r\n    }\r\n}",
  CURLOPT_HTTPHEADER => array(
    "Authorization: key=AAAAftAB2zY:APA91bEEGkoeeyHY31bqkoYw5EnPFgQl4SFKdtjxmaLYzc6RVOmooDw6pTpuzCoSzmHauxx3jdsprSdQ6Vzf8q9kpV7Zo02P0ykphPsquyqD-H-1cK4gZIWWscaJrn2pyM9dXpFiSiyf",
    "Content-Type: application/json",
    "Postman-Token: 195ee532-fba9-4071-a7c3-10ad529524eb",
    "cache-control: no-cache"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  echo $response;
}
?>